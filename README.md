# SUML: Simple & Unambiguous Markup Language

![SUML logo](logo-big.png)

YAML gets praised for being clear and human readable, but it's also criticised for being ambiguous.

SUML is an attempt to keep the good things about YAML but remove its ambiguity and needless complexity.

## Language overview

 - SUML is a basically a stricter subset of YAML with fewer features, which means a valid SUML document is also
   a valid YAML document (but not the other way around). That means for example that you can use
   regular YAML syntax highlighting also for SUML files.
 - SUML is indentation-based and it's strict about it: always four spaces
 - UTF-8 encoding is required
 - Single-line strings have to be quoted with `'`
 - `.suml` file extension is suggested
 - everything from a `#` character until the end of line is a comment

## Data types

### Null

Null is represented as `~`

### Number

Numbers can be represented as integers (`-123`), floats (`-123.45`), in exponential notation (`-1.2345e2`),
as a hexadecimal (`-0x7B`), octal (`-0o173`) or binary number (`-0b1111011`), or infinity (`-inf`)

## String

If strings are one-line, they have to be quoted with `'`, for example; `'text'`.
Single quotes inside strings can be escaped by doubling them: `'no, she doesn''t do that'`

To keep all the whitespaces and new lines in a text,
start a block with `|` and indent the following lines one lever deeper:

    foo: 'FOO'
    bar: |
        <strong>OK</strong>
        
        this text: won't be parsed, it will be treated as raw text
    baz: 'BAZ'

To split a long string into multiple lines visually (but keep parsing it as a single line),
start a block with `>` and indent the following lines one lever deeper:

    foo: 'FOO'
    bar: >
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae massa at nulla sollicitudin sagittis vel
        a nisl. Suspendisse potenti. Suspendisse condimentum ante et nulla molestie ornare. Nam convallis vehicula
        nisl id scelerisque. Donec accumsan dolor in augue blandit bibendum. Nam sit amet congue odio. Proin
        faucibus rutrum dui eu rhoncus.
    baz: 'BAZ'

### Boolean

Either `true` or `false`

### Datetime

SUML accepts format `Y-m-d H:i:sO`, with both date and time(zone) being optional, for example:
`2019-03-12`, `2019-03-12 17:58:00`, `2019-03-12 17:58:00 CET`, `17:58:00 CET`.

If you start an integer with `@`, SUML will treat it as Unix timestamp: `@1552413480`

### List

List elements start with `-`.

    - 'foo'
    - 'bar'
    -
        - 'nested'
        - true
    - 8

Inline syntax: `['foo', 8, true]`

### Dict

Dictionary items have a format `key: value`.
Key is unquoted and will be treated as a string. It can't contain `:`, `#`, `'`, or spaces.

    foo: 'FOO'
    bar: 'BAR'
    nested:
        lorem: 8
        ipsum: 17
        list:
            - 'foo'
            - 'bar'

Inline syntax: `{foo: 'foo', bar: 8, bool: true}`

## Example

    null: ~
    
    numbers:
        integer: 123 # foo'foo#foo
        decimal: -12.5
        exp: 0.14e-8
        bin: 0b1001101
        oct: 0o755
        hex: -0xc42560
        inf: inf
        -inf: -inf
        zero: 0
    # full line comment
      # full line comment with spaces
    strings:
        foobar: 'lorem ipsum dolor sit amet 😈 unicode'
        quoted: 'foo "bar" b:az, it doesn''t' # end of line comment with quote ' and hash # chars
        block: |
            foo: 'bar'
              indented
    
            test: <strong>ok</strong>
        folded-block: >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis id metus tellus.
            Quisque dignissim quam id #ullamcorper rhoncus. Cras ut orci viverra, laoreet purus ut, dictum turpis.
            Curabitur et porttitor mi. Nulla condimentum ex eget dignissim commodo. In hac habitasse platea dictumst.
    
            Mauris a porta diam. Duis mollis neque odio, ut malesuada leo vestibulum vel. Ut luctus faucibus libero vitae malesuada.
            Suspendisse nunc lectus, euismod eu tempus bibendum, finibus eu purus.
        not-null: '~'
        not-integer: '123'
        not-decimal: '-12.5'
        not-exp: '0.14e-8'
        not-bin: '0b1001110'
        not-oct: '0o755'
        not-hex: '-0xc42560'
        not-inf: '-Inf'
        not-zero: '0'
        not-bool: 'true'
        not-date: '2019-03-12'
        nested:
            foo: 'bar'
    
    
    list:
        - 'element'
        - 8
        - ~
    bool:
        true: true
        false: false
    datetime:
        date: 2019-03-12
        time: 17:58:00
        time-tz: 17:58:00+0100
        datetime: 2019-03-12 17:58:00
        datetime-tz: 2019-03-12 17:58:00+0100
        timestamp: @1552413480

## Implementations:

 - [PHP](https://gitlab.com/Avris/SUML-PHP) (+ [Symfony support](https://gitlab.com/Avris/SUML-symfony))
 - [JavaScript](https://gitlab.com/Avris/SUML-JS) (+ [Webpack loader](https://gitlab.com/Avris/SUML-Loader))

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
